+++
title = "Gitlab news 2019"
outputs = ["Reveal"]
+++

# Nouveautés de la forge Gitlab

Jacques Supcik, 8 Novembre 2019

<img src="img/isis.svg" style="width: 20rem; box-shadow: none;"/>

---

# CI/CD

Construction, tests, déployements en utilisant des "runners"

---

# Pages

Sites web statique.

---

# Registry

Dépot pour les images Docker
